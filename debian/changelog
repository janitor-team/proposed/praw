praw (7.2.0-1) experimental; urgency=medium

  * New upstream release (7.2.0)

 -- Josue Ortega <josue@debian.org>  Thu, 04 Mar 2021 19:17:04 -0600

praw (7.1.4-1) unstable; urgency=medium

  * New upstream release (7.1.4)

 -- Josue Ortega <josue@debian.org>  Sat, 13 Feb 2021 17:16:47 -0600

praw (7.1.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Josue Ortega ]
  * New upstream release (7.1.0)
  * Bump debhelper-compat to version 13
  * Bump Standards-Version to 4.5.1. No changes required
  * Bump debian/watch standard to use 4
  * Mark autopkgtest as superficial (Closes: #970961)
  * debian/rules: Add -f option to rm called during the override_dh_auto_build
    stage

 -- Josue Ortega <josue@debian.org>  Wed, 30 Dec 2020 17:25:47 -0600

praw (6.1.1-2) unstable; urgency=medium

  * Change build dependency on python-six to use python3 version.
    Closes: #943175
  * Update package to use debhelper-compat (= 12)
  * Bump Standards-Version to 4.4.1

 -- Josue Ortega <josue@debian.org>  Tue, 12 Nov 2019 20:28:57 -0600

praw (6.1.1-1) unstable; urgency=medium

  * New upstream release (6.1.1)
  * debian/control: Add python3-websocket both as build and package
    dependency.
  * Bump Standards-Version to 4.3.0:
    - debian/control: Document Rules-Requires-Root field.
  * debian/rules: Remove unnecessary get-orig-source target
  * Add examples installation.
  * Add debian CI tests.

 -- Josue Ortega <josue@debian.org>  Sat, 09 Feb 2019 11:25:43 -0600

praw (6.0.0-1) unstable; urgency=medium

  [Ondřej Nový]
  * d/control: Set Vcs-* to salsa.debian.org
  * Convert git repository from git-dpm to gbp layout

  [Josue Ortega]
  * New upstream release (6.0.0).
  * debian/source/options: Add rule to ignore fonts installation for
    praw-doc package.
  * Bumps Standards-Version to 4.2.0:
    - Increase debian/rules build verbosity.

 -- Josue Ortega <josue@debian.org>  Sun, 26 Aug 2018 17:47:03 -0600

praw (5.3.0-1) unstable; urgency=medium

  * New upstream release (5.3.0)
  * Bumps debhelper compat version to 11.
  * debian/control:
    - Bumps dependency on python3-prawcore to 0.13.0.
    - Bumps Standards-Version to 4.1.3, no changes required.
  * Removes installation ofr praw-multiprocess manpage.
  * praw-doc.info: Removes installation of PNG files, they are no longer
    shipped.

 -- Josue Ortega <josue@debian.org>  Sun, 28 Jan 2018 16:48:18 -0600

praw (5.2.0-1) unstable; urgency=medium
  * New upstream release.
  * New maintainer (Closes: #855636).
  * debian/control:
    - Updates Uploaders and Mainter field.
    - Removes python 2 package.
    - Adds python3-prawcore as dependency.
    - Adds python3-sphinx-rtd-theme as build dependency.
    - Updates Vcs-* fields to use the new GIT repository.
    - Adds python3-pytest-runner as build dependency.
  * Bumps debian/compat version from 9 to 10.
  * debian/watch: Changes URI to use HTTPS.
  * debian/rules:
    - Removes python2 from `dh --with` argument.
    - Removes override_dh_auto_install, praw-multiprocess is
      no longer generated.
  * Removes lintian-overrides.
  * Updates debian/copyright, upstream source code has been relicensed
    to BSD-2-Clause.
  * Bumps Standards-Version to 4.1.1, debian/copyright Format field changed to
    its HTTPS form.
  * debian/praw-doc.doc base, removes *.png files references.

 -- Josue Ortega <josue@debian.org>  Sat, 28 Oct 2017 17:19:10 -0600

praw (3.3.0-1) unstable; urgency=high

  * New upstream release
  * Removed debian/patches/change-info-section.patch, as the changes
    have been integrated upstream
  * Disabled tests (Closes: #805226)

 -- Riley Baird <BM-2cVqnDuYbAU5do2DfJTrN7ZbAJ246S4XiX@bitmessage.ch>  Sun, 11 Oct 2015 15:47:27 +1100

praw (3.2.1-1) unstable; urgency=medium

  * New upstream release
  * Updated patches
  * Added Ethan Dalool and Jonathan Stein to d/copyright

 -- Riley Baird <BM-2cVqnDuYbAU5do2DfJTrN7ZbAJ246S4XiX@bitmessage.ch>  Sun, 20 Sep 2015 08:24:53 +1000

praw (3.2.0-1) unstable; urgency=low

  * Initial release (Closes: #728946)

 -- Riley Baird <BM-2cVqnDuYbAU5do2DfJTrN7ZbAJ246S4XiX@bitmessage.ch>  Mon, 15 Jun 2015 19:53:32 +1000
