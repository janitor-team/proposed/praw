Source: praw
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Josue Ortega <josue@debian.org>
Build-Depends: debhelper-compat (= 13),
 python3,
 python3-setuptools,
 dh-python,
 python3-decorator (>=3.4.2),
 python3-requests (>=2.3.0),
 python3-six (>=1.4),
 python3-sphinx,
 python3-sphinx-rtd-theme,
 python3-prawcore,
 python3-pytest-runner,
 texinfo,
 python3-websocket
Standards-Version: 4.5.1
Homepage: https://praw.readthedocs.org/
Vcs-Git: https://salsa.debian.org/python-team/packages/praw.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/praw
Rules-Requires-Root: no

Package: python3-praw
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
 python3-prawcore (>= 0.13.0),
 python3-websocket
Recommends: praw-doc
Description: Python Reddit API Wrapper (Python 3 version)
 PRAW, an acronym for "Python Reddit API Wrapper", is a Python module that
 allows for simple access to Reddit's API. PRAW aims to be as easy to use as
 possible and is designed to follow all of Reddit's API rules.
 .
 This package provides the Python 3 version.

Package: praw-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, install-info
Recommends: libjs-jquery, libjs-underscore
Suggests: python3-praw
Description: Python Reddit API Wrapper (Documentation)
 PRAW, an acronym for "Python Reddit API Wrapper", is a Python module that
 allows for simple access to Reddit's API. PRAW aims to be as easy to use as
 possible and is designed to follow all of Reddit's API rules.
 .
 This package provides documentation and tests for PRAW.
